# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: adubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/01/23 12:05:01 by adubois           #+#    #+#              #
#*   Updated: 2016/04/01 23:11:05 by adubois          ###   ########.fr       *#
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
CC = gcc
CFLAGS = -Wall -Werror -Wextra
INC_PATH = -I./inc
SRC_PATH = ./src
OBJ_PATH = ./obj

SRC = ft_printf.c \
	  conversion.c \
	  conversion_functions.c \
	  conversion_functions_integers.c \
	  conversion_get.c \
	  conversion_get_args.c \
	  error.c \
	  format.c \
	  format_conversion.c \
	  format_conversion_padding.c \
	  helpers.c \
	  result.c \
	  u8_string.c \
	  helpers/ft_abs.c \
	  helpers/ft_atoi.c \
	  helpers/ft_isdigit.c \
	  helpers/ft_islower.c \
	  helpers/ft_isspace.c \
	  helpers/ft_isupper.c \
	  helpers/ft_itoa_base.c \
	  helpers/ft_memcpy.c \
	  helpers/ft_memset.c \
	  helpers/ft_strcat.c \
	  helpers/ft_strchr.c \
	  helpers/ft_strchrpos.c \
	  helpers/ft_strcmp.c \
	  helpers/ft_strcpy.c \
	  helpers/ft_strdup.c \
	  helpers/ft_strjoin.c \
	  helpers/ft_strlen.c \
	  helpers/ft_strncmp.c \
	  helpers/ft_strncpy.c \
	  helpers/ft_strnew.c \
	  helpers/ft_strrev.c \
	  helpers/ft_strsub.c \
	  helpers/ft_strtolower.c \
	  helpers/ft_strtoupper.c \
	  helpers/ft_tolower.c \
	  helpers/ft_toupper.c \
	  helpers/ft_utoa_base.c \
	  helpers/ft_vector.c \
	  helpers/ft_wcslen.c \
	  helpers/ft_wcstombs.c \
	  helpers/ft_wctomb.c

OBJ = $(SRC:%.c=$(OBJ_PATH)/%.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@mkdir -p $(OBJ_PATH)/helpers
	$(CC) $(CFLAGS) $(INC_PATH) -o $@ -c $<

clean:
	rm -rf $(OBJ_PATH)

fclean: clean
	rm -rf $(NAME)

re: fclean all

.PHONY: all dirs clean fclean re
