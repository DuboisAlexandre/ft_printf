/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/23 11:59:51 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:24:49 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>
# include <wchar.h>

# define DEFAULT_VECTOR_SIZE 4
# define DEFAULT_PRECISION 6
# define ERROR_FORMAT "Format error"
# define ERROR_CONVERSION "Conversion error"
# define SPECIFIERS "spdiouxcn%"
# define MODIFIERS "hljz"
# define FLAGS "#0- +"

/*
** You can invert flags has you want but don't change the specifiers.
*/

typedef struct	s_vector
{
	int		index;
	int		size;
	void	**cells;
}				t_vector;

typedef struct	s_u8_string
{
	int		size;
	char	*str;
}				t_u8_string;

typedef struct	s_conversion
{
	unsigned char	flags;
	char			modifier;
	int				arg_index;
	int				width;
	int				width_arg;
	int				precision;
	int				precision_arg;
	int				uppercase;
	char			specifier;
}				t_conversion;

typedef struct	s_result
{
	int			size;
	int			manual_args;
	va_list		ap;
	va_list		ap_bkp;
	char		*format;
	char		*format_ptr;
	t_vector	*strings;
	t_vector	*convs;
	int			(*fn[10])(struct s_result *result, t_conversion *conv,
							char **str);
}				t_result;

/*
** Main functions
*/

int				ft_vasprintf(char **ret, const char *format, va_list ap);
int				ft_printf(const char *format, ...);

/*
** Structures management
*/

t_vector		*ft_vector_create(int size);
void			ft_vector_add(t_vector *vector, void *ptr);
void			ft_vector_resize(t_vector *vector);
void			ft_vector_destroy(t_vector **vector);

t_u8_string		*u8_string_new(char *str, int size);
void			u8_string_del(t_u8_string **str);

t_conversion	*conversion_new(void);
void			conversion_del(t_conversion **conv);

t_result		*result_create(const char *format, va_list ap);
void			result_destroy(t_result **result);
char			*result_compose(t_result *result);

/*
** Parsing
*/

int				format_parse(t_result *result);
int				format_parse_string(t_result *result);
int				format_parse_conversion(t_result *result);

/*
** Conversion management
*/

void			format_conversion(t_u8_string *mbs, t_conversion *conv);
void			format_conversion_alternate(t_u8_string *mbs,
											t_conversion *conv);
void			format_conversion_sign(t_u8_string *mbs, t_conversion *conv);
void			format_conversion_padding(t_u8_string *mbs, t_conversion *conv);
void			format_conversion_precision(t_u8_string *mbs,
											t_conversion *conv);

int				conversion_compute(t_result *result);
int				conversion_dispatch(t_conversion *conv, char **str,
									t_result *result);
void			conversion_load(t_result *result);

int				conversion_get_arg_index(t_result *result);
int				conversion_get_arg(t_conversion *conv, t_result *result);
int				conversion_get_flags(t_conversion *conv, t_result *result);
int				conversion_get_width(t_conversion *conv, t_result *result);
int				conversion_get_precision(t_conversion *conv, t_result *result);
int				conversion_get_modifier(t_conversion *conv, t_result *result);
int				conversion_get_specifier(t_conversion *conv, t_result *result);

/*
** Conversion functions
*/

int				conversion_fn_s(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_p(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_d(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_o(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_u(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_x(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_c(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_n(t_result *result, t_conversion *conv,
								char **str);
int				conversion_fn_percent(t_result *result, t_conversion *conv,
										char **str);

/*
** Error handling
*/

int				ft_printf_error(t_result **result, char *str, int error_code);

/*
** Helpers
*/

void			forward_args(va_list ap, int steps);
void			reset_args(t_result *result);
void			reset_forward_args(t_result *result, int steps);
void			get_width_precision(t_result *result, t_conversion *conv);
char			*get_padding(int size, char c);

int				ft_abs(int n);
int				ft_atoi(const char *str);
char			*ft_strcat(char *s1, const char *s2);
int				ft_isdigit(int c);
int				ft_isspace(int c);
int				ft_islower(int c);
int				ft_isupper(int c);
char			*ft_itoa(int n);
char			*ft_itoa_base(long int n, int base);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memset(void *b, int c, size_t len);
char			*ft_strchr(const char *s, int c);
int				ft_strchrpos(const char *str, char chr);
int				ft_strcmp(const char *s1, const char *s2);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strdup(const char *s1);
unsigned int	ft_strlen(const char *str);
char			*ft_strjoin(char const *s1, char const *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
char			*ft_strncpy(char *dst, const char *src, size_t n);
char			*ft_strnew(size_t size);
char			*ft_strrev(char *str);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strtolower(char *str);
char			*ft_strtoupper(char *str);
int				ft_tolower(int c);
int				ft_toupper(int c);
char			*ft_utoa_base(unsigned long int n, int base);
int				ft_wcslen(wchar_t *wcs);
int				ft_wcstombs(wchar_t *tab, char **str, int size);
int				ft_wctomb(wchar_t chr, char *str);

#endif
