/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   result.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 19:08:05 by adubois           #+#    #+#             */
/*   Updated: 2016/04/02 00:00:40 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Creates and initializes a new t_result data strucure and returns a pointer
** to it.
*/

t_result	*result_create(const char *format, va_list ap)
{
	t_result	*result;

	if (!(result = (t_result*)malloc(sizeof(t_result))))
		exit(-1);
	result->size = 0;
	result->manual_args = -1;
	va_copy(result->ap, ap);
	va_copy(result->ap_bkp, ap);
	result->format = ft_strdup(format);
	result->format_ptr = result->format;
	result->strings = ft_vector_create(DEFAULT_VECTOR_SIZE);
	result->convs = ft_vector_create(DEFAULT_VECTOR_SIZE);
	return (result);
}

/*
** Destroys the the given t_result data structure.
*/

void		result_destroy(t_result **result)
{
	t_result		*res;
	t_conversion	*conv;
	t_u8_string		*mbs;
	int				i;

	res = *result;
	va_end(res->ap);
	i = 0;
	while (res->convs->cells[i])
	{
		conv = (t_conversion *)res->convs->cells[i++];
		conversion_del(&conv);
	}
	ft_vector_destroy(&(res->convs));
	i = 0;
	while (res->strings->cells[i])
	{
		mbs = (t_u8_string *)res->strings->cells[i++];
		u8_string_del(&mbs);
	}
	ft_vector_destroy(&(res->strings));
	free(res->format_ptr);
	free(res);
	*result = NULL;
}

/*
** Creates the result string from the original and converted strings
** and returns a pointer to the result string.
*/

char		*result_compose(t_result *result)
{
	t_u8_string	*u8_str;
	char		*str;
	int			i;
	int			offset;

	if (!(str = (char*)malloc(sizeof(char) * (result->size + 1))))
		exit(-1);
	offset = 0;
	i = 0;
	while (i < result->strings->index - 1)
	{
		u8_str = (t_u8_string*)result->strings->cells[i++];
		if (u8_str)
		{
			ft_memcpy(str + offset, u8_str->str, u8_str->size);
			offset += u8_str->size;
		}
	}
	return (str);
}
