/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 12:57:54 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:57:04 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Creates and initializes a new t_conversion data structure and returns a
** pointer to it.
*/

t_conversion	*conversion_new(void)
{
	t_conversion	*conv;

	if (!(conv = (t_conversion*)malloc(sizeof(t_conversion))))
		exit(-1);
	conv->flags = 0;
	conv->modifier = '\0';
	conv->arg_index = 0;
	conv->width = 0;
	conv->width_arg = 0;
	conv->precision = -2;
	conv->precision_arg = 0;
	conv->uppercase = 0;
	conv->specifier = '\0';
	return (conv);
}

/*
** Deletes the given t_conversion data structure.
*/

void			conversion_del(t_conversion **conv)
{
	free(*conv);
	*conv = NULL;
}

/*
** Manages the computing of all the conversions, picking them one by one
** and storing the result in the strings array.
*/

int				conversion_compute(t_result *result)
{
	int				i;
	int				size;
	char			*str;
	t_conversion	*conv;
	t_u8_string		*u8_str;

	i = 0;
	while (result->convs->cells[i])
	{
		conv = (t_conversion*)result->convs->cells[i];
		u8_str = (t_u8_string*)result->strings->cells[i << 1];
		result->size += u8_str->size;
		if (-1 == (size = conversion_dispatch(conv, &str, result)))
			return (-1);
		u8_str = u8_string_new(str, size);
		format_conversion(u8_str, conv);
		result->strings->cells[(i << 1) + 1] = u8_str;
		result->size += u8_str->size;
		if (result->manual_args)
			reset_args(result);
		++i;
	}
	u8_str = (t_u8_string*)result->strings->cells[result->strings->index - 2];
	result->size += u8_str->size;
	return (0);
}

/*
** Redirects the given conversion to the right function.
*/

int				conversion_dispatch(t_conversion *conv, char **str,
									t_result *result)
{
	char	*specifiers;
	int		pos;

	specifiers = SPECIFIERS;
	if (-1 == (pos = ft_strchrpos(specifiers, conv->specifier)))
		pos = ft_strchrpos(specifiers, 'c');
	get_width_precision(result, conv);
	if (conv->arg_index > 0)
		reset_forward_args(result, conv->arg_index);
	return (result->fn[pos](result, conv, str));
}

/*
** Loads all the conversion functions.
*/

void			conversion_load(t_result *result)
{
	result->fn[0] = conversion_fn_s;
	result->fn[1] = conversion_fn_p;
	result->fn[2] = conversion_fn_d;
	result->fn[3] = conversion_fn_d;
	result->fn[4] = conversion_fn_o;
	result->fn[5] = conversion_fn_u;
	result->fn[6] = conversion_fn_x;
	result->fn[7] = conversion_fn_c;
	result->fn[8] = conversion_fn_n;
	result->fn[9] = conversion_fn_percent;
}
