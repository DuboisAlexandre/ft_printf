/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   u8_string.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 19:36:45 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:56:08 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Creates a new t_u8_string data structure, initializes it with the
** given data and returns a pointer to it.
*/

t_u8_string	*u8_string_new(char *str, int size)
{
	t_u8_string	*u8_str;

	if (!(u8_str = (t_u8_string*)malloc(sizeof(t_u8_string))))
		exit(-1);
	u8_str->str = str;
	u8_str->size = size;
	return (u8_str);
}

/*
** Destroys the given t_u8_string data structure.
*/

void		u8_string_del(t_u8_string **str)
{
	if ((*str)->str)
		free((*str)->str);
	free(*str);
	*str = NULL;
}
