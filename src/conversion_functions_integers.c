/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_functions_integers.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 21:51:09 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:36:50 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Conversion functions for the integers types:
** signed decimal, unsigned octal, unsigned decimal and unsigned hexadecimal.
*/

int			conversion_fn_d(t_result *result, t_conversion *conv, char **str)
{
	int		base;

	base = 10;
	if (conv->modifier == '\0' && !conv->uppercase)
		*str = ft_itoa_base(va_arg(result->ap, int), base);
	else if (conv->modifier == 'H')
		*str = ft_itoa_base((signed char)va_arg(result->ap, int), base);
	else if (conv->modifier == 'h')
		*str = ft_itoa_base((short)va_arg(result->ap, int), base);
	else
		*str = ft_itoa_base(va_arg(result->ap, long), base);
	return (ft_strlen(*str));
}

int			conversion_fn_o(t_result *result, t_conversion *conv, char **str)
{
	int		base;

	base = 8;
	if (conv->modifier == '\0' && !conv->uppercase)
		*str = ft_utoa_base((unsigned int)va_arg(result->ap, int), base);
	else if (conv->modifier == 'H')
		*str = ft_utoa_base((unsigned char)va_arg(result->ap, int), base);
	else if (conv->modifier == 'h')
		*str = ft_utoa_base((unsigned short)va_arg(result->ap, int), base);
	else
		*str = ft_utoa_base((unsigned long)va_arg(result->ap, long), base);
	return (ft_strlen(*str));
}

int			conversion_fn_u(t_result *result, t_conversion *conv, char **str)
{
	int		base;

	base = 10;
	if (conv->modifier == '\0' && !conv->uppercase)
		*str = ft_utoa_base((unsigned int)va_arg(result->ap, int), base);
	else if (conv->modifier == 'H')
		*str = ft_utoa_base((unsigned char)va_arg(result->ap, int), base);
	else if (conv->modifier == 'h')
		*str = ft_utoa_base((unsigned short)va_arg(result->ap, int), base);
	else
		*str = ft_utoa_base((unsigned long)va_arg(result->ap, long), base);
	return (ft_strlen(*str));
}

int			conversion_fn_x(t_result *result, t_conversion *conv, char **str)
{
	int		base;

	base = 16;
	if (conv->modifier == '\0')
		*str = ft_utoa_base((unsigned int)va_arg(result->ap, int), base);
	else if (conv->modifier == 'H')
		*str = ft_utoa_base((unsigned char)va_arg(result->ap, int), base);
	else if (conv->modifier == 'h')
		*str = ft_utoa_base((unsigned short)va_arg(result->ap, int), base);
	else
		*str = ft_utoa_base((unsigned long)va_arg(result->ap, long), base);
	if (conv->uppercase)
		ft_strtoupper(*str);
	else
		ft_strtolower(*str);
	return (ft_strlen(*str));
}
