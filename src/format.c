/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 19:27:49 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:59:03 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** The main parsing function calls repeatedly the string parsing and
** conversion parsing functions until the end of the format.
*/

int		format_parse(t_result *result)
{
	while (-1 != format_parse_string(result))
		if (-1 == format_parse_conversion(result))
			return (-1);
	return (0);
}

/*
** Grabs the string, from the current position in the format string,
** to the next conversion character and stores it in the strings array.
*/

int		format_parse_string(t_result *result)
{
	char		*str;
	t_u8_string	*u8_str;
	int			len;

	if (-1 == (len = ft_strchrpos(result->format, '%')))
		str = ft_strsub(result->format, 0, ft_strlen(result->format));
	else
		str = ft_strsub(result->format, 0, len);
	u8_str = u8_string_new(str, ft_strlen(str));
	ft_vector_add(result->strings, (void*)u8_str);
	ft_vector_add(result->strings, NULL);
	result->format += len;
	return (len);
}

/*
** Grabs the next conversion and stores it the conversions array.
*/

int		format_parse_conversion(t_result *result)
{
	t_conversion	*conv;

	result->format++;
	conv = conversion_new();
	if (-1 == conversion_get_arg(conv, result) ||
		-1 == conversion_get_flags(conv, result) ||
		-1 == conversion_get_width(conv, result) ||
		-1 == conversion_get_precision(conv, result) ||
		-1 == conversion_get_modifier(conv, result) ||
		-1 == conversion_get_specifier(conv, result))
		return (-1);
	ft_vector_add(result->convs, (void*)conv);
	if (result->manual_args == -1)
		result->manual_args = 0;
	return (0);
}
