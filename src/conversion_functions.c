/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_functions.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 12:49:42 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:35:31 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Conversion functions for the strings, pointers, chars and percent.
** 'n' is not a real conversion but is technically part of it.
*/

int			conversion_fn_s(t_result *result, t_conversion *conv, char **str)
{
	void	*arg;
	int		ret;

	arg = va_arg(result->ap, void*);
	if (!arg)
		*str = ft_strdup("(null)");
	else if (conv->uppercase || conv->modifier == 'l')
		return ((ret = ft_wcstombs((wchar_t*)arg, str, conv->precision)));
	else
		*str = ft_strdup((char*)arg);
	ret = ft_strlen(*str);
	if (conv->precision >= 0 && conv->precision < ret)
	{
		(*str)[conv->precision] = '\0';
		ret -= ret - conv->precision;
	}
	return (ret);
}

int			conversion_fn_p(t_result *result, t_conversion *conv, char **str)
{
	*str = ft_utoa_base((long int)va_arg(result->ap, void*), 16);
	ft_strtolower(*str);
	conv->flags |= 1 << (7 - ft_strchrpos(FLAGS, '#'));
	return (ft_strlen(*str));
}

int			conversion_fn_c(t_result *result, t_conversion *conv, char **str)
{
	int		ret;

	if (conv->arg_index >= 0 && (conv->uppercase || conv->modifier == 'l'))
	{
		*str = (char*)malloc(sizeof(char) * (4 + 1));
		ret = ft_wctomb((wchar_t)va_arg(result->ap, wint_t), *str);
		if (ret == -1)
			return (-1);
		(*str)[ret] = '\0';
	}
	else
	{
		*str = (char*)malloc(sizeof(char) * (1 + 1));
		if (conv->arg_index == -1)
			(*str)[0] = *result->format;
		else
			(*str)[0] = (unsigned char)va_arg(result->ap, int);
		(*str)[1] = '\0';
		if (*result->format != '\0')
			ret = 1;
		else
			ret = 0;
	}
	return (ret);
}

int			conversion_fn_n(t_result *result, t_conversion *conv, char **str)
{
	(void)conv;
	(void)str;
	if (conv->modifier == 'H')
		*(va_arg(result->ap, signed char*)) = result->size;
	else if (conv->modifier == 'h')
		*(va_arg(result->ap, short*)) = result->size;
	else if (conv->modifier == 'l' || conv->modifier == 'j' ||
			conv->modifier == 'z')
		*(va_arg(result->ap, long*)) = result->size;
	else if (conv->modifier == 'L')
		*(va_arg(result->ap, long long*)) = result->size;
	else
		*(va_arg(result->ap, int*)) = result->size;
	*str = (char*)malloc(sizeof(char));
	(*str)[0] = '\0';
	return (0);
}

int			conversion_fn_percent(t_result *result, t_conversion *conv,
									char **str)
{
	(void)result;
	*str = (char*)malloc(sizeof(char) * (1 + 1));
	(*str)[0] = '%';
	(*str)[1] = '\0';
	conv->specifier = 'c';
	return (1);
}
