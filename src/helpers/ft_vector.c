/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/29 17:36:47 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 15:00:42 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_vector	*ft_vector_create(int size)
{
	t_vector	*vector;

	if (!(vector = (t_vector*)malloc(sizeof(t_vector))))
		exit(-1);
	vector->index = 0;
	vector->size = size;
	if (!(vector->cells = (void**)malloc(sizeof(void*) * size)))
		exit(-1);
	while (--size >= 0)
		vector->cells[size] = NULL;
	return (vector);
}

void		ft_vector_add(t_vector *vector, void *ptr)
{
	if (vector->index + 1 == vector->size)
		ft_vector_resize(vector);
	vector->cells[vector->index++] = ptr;
}

void		ft_vector_resize(t_vector *vector)
{
	void		**cells;
	int			i;

	if (!(cells = (void**)malloc(sizeof(void*) * vector->size * 2)))
		exit(-1);
	i = 0;
	while (i < vector->size)
	{
		cells[i] = vector->cells[i];
		i++;
	}
	vector->size *= 2;
	while (i < vector->size)
		cells[i++] = NULL;
	free(vector->cells);
	vector->cells = cells;
}

void		ft_vector_destroy(t_vector **vector)
{
	free((*vector)->cells);
	free(*vector);
	*vector = NULL;
}
