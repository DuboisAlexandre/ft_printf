/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 19:03:03 by adubois           #+#    #+#             */
/*   Updated: 2016/03/25 20:08:03 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_strdup(const char *s1)
{
	char	*dest;

	if ((dest = (char*)malloc(ft_strlen(s1) + 1)) == NULL)
		return (NULL);
	return (ft_strcpy(dest, s1));
}
