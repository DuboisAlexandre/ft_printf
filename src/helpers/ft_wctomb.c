/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wctomb.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 15:52:46 by adubois           #+#    #+#             */
/*   Updated: 2016/03/31 15:00:15 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	conv_to_multibyte(int bytes, wchar_t chr, char *str)
{
	int		offset;

	offset = 0;
	while (++offset < bytes)
		str[offset] = 0x80 | (chr >> ((bytes - offset - 1) * 6) & 0x3F);
	--offset;
	if (bytes == 1)
		str[0] = (unsigned char)chr;
	else if (bytes == 2)
		str[0] = 0xC0 | (chr >> (offset * 6) & 0x1F);
	else if (bytes == 3)
		str[0] = 0xE0 | (chr >> (offset * 6) & 0xF);
	else if (bytes == 4)
		str[0] = 0xF0 | (chr >> (offset * 6) & 0x7);
	return (bytes);
}

int			ft_wctomb(wchar_t chr, char *str)
{
	int		i;
	int		count;

	if ((chr >= 0 && chr <= 0xD7FF) || (chr >= 0xE000 && chr <= 0x10FFFF))
	{
		count = 1;
		i = 7;
		while (i < 22)
		{
			if (chr >> i == 0)
				return (conv_to_multibyte(count, chr, str));
			i += 4 + (i > 7);
			++count;
		}
	}
	return (-1);
}
