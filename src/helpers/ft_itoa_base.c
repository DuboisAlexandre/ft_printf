/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 13:15:57 by adubois           #+#    #+#             */
/*   Updated: 2016/03/31 19:32:58 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

char	*ft_itoa_base(long int n, int base)
{
	int		i;
	int		negative;
	char	*number;
	char	*charset;

	charset = "0123456789abcdef";
	if (base < 2 || base > 16)
		return (NULL);
	negative = (n < 0) ? 1 : 0;
	if (!(number = (char*)malloc(sizeof(char) * (32 + 1))))
		return (NULL);
	i = 0;
	if (n == 0)
		number[i++] = '0';
	while (n != 0)
	{
		number[i] = charset[ft_abs(n % base)];
		n /= base;
		i++;
	}
	if (negative)
		number[i++] = '-';
	number[i] = '\0';
	return (ft_strrev(number));
}
