/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wcstombs.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/25 19:36:12 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 22:47:36 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			ft_wcstombs(wchar_t *tab, char **str, int size)
{
	char			*tmp;
	int				offset;
	unsigned int	i;
	int				result;

	if (!(tmp = (char*)malloc(sizeof(char) * (ft_wcslen(tab) * 4 + 1))))
		exit(-1);
	offset = 0;
	i = 0;
	while (tab[i] && (size == -2 || offset < size))
	{
		if (-1 == (result = ft_wctomb(tab[i], tmp + offset)))
			return (-1);
		if (size == -2 || offset + result <= size)
			offset += result;
		else
			break ;
		i++;
	}
	tmp[offset] = '\0';
	*str = tmp;
	return (offset);
}
