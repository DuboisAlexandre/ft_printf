/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conversion_get_args.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 21:57:37 by adubois           #+#    #+#             */
/*   Updated: 2016/04/01 23:57:50 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Grabs the index of the argument to use in the conversion. The index has
** to be the next information to read.
*/

int			conversion_get_arg_index(t_result *result)
{
	char	*format;
	int		nb;

	format = result->format;
	while (ft_isdigit(*format))
		++format;
	if (*format != '$' && result->manual_args == -1)
		return (0);
	if ((*format != '$' && result->manual_args == 1) ||
		(*format == '$' && result->manual_args == 0))
		return (-1);
	if (result->manual_args == -1)
		result->manual_args = (*format == '$') ? 1 : 0;
	if (result->manual_args == 0)
		return (0);
	if (0 == (nb = ft_atoi(result->format)))
		return (-1);
	result->format = format + 1;
	return (nb);
}

int			conversion_get_arg(t_conversion *conv, t_result *result)
{
	if (-1 == (conv->arg_index = conversion_get_arg_index(result)))
		return (-1);
	return (0);
}
